import time
import datetime
from threading import Thread
 #will work when running from app


#initialize pmcontroller
pmcontroller = None

try:
    from src.pmcontroller import PMController
    pmcontroller = PMController()
except Exception as e:
    print "couldn't load pmcontroller"
    print e


class ScheduleRunner:

    def __init__(self, db):
        self.startAdvance = 30 #in minutes, start before starttime
        self.endAdvance = 20 #in minutes, stop before endtime
        self.db = db
        self.running = True
        self.todaysWeekday = datetime.datetime.today().weekday()
        self.pauseInterval = 5 #seconds in between calls


    def stop(self):
        self.running = False

    def getAllSchedules(self):
        self.allSchedules = self.db.getAll()

    def setTodaysSchedules(self):


        todaysSchedules = []
        for schedule in self.allSchedules:
            if schedule[1] == self.todaysWeekday:
                todaysSchedules.append(schedule)


        self.todaysSchedules = todaysSchedules

    def runThroughTodaysSchedules(self):
        print "run through todays schedules: %s" %self.todaysSchedules

        now = datetime.datetime.now().time()

        count = 0   #check how many times the time was checked

        for schedule in self.todaysSchedules:
            #get parameters
            starttime = schedule[2]
            endtime = schedule[3]
            temp = schedule[4]
            socket = schedule[5]

            # calculate when to start, beforehand, to have the right temperature when needed
            adj_starttime = (datetime.datetime.strptime(starttime,"%H:%M") - datetime.timedelta(minutes=self.startAdvance)).time()
            print "adj starttime is %s" %adj_starttime

            adj_endtime = (datetime.datetime.strptime(endtime,"%H:%M") - datetime.timedelta(minutes=self.endAdvance)).time()
            print "adj endtime is %s, %s" %(adj_endtime,type(adj_endtime))


            print "starttime: %s" %starttime
            print "endtime: %s" %endtime

            # run through all schedules, and turn on if needed

            if now > adj_starttime and now < adj_endtime:
                if 1 == 1: # place holder for checking current temp regarding target temp ex. currenttemp < targettemp

                    print "turning on!"
                    #include pmcontroller call for on
                    try:
                        pmcontroller.turnOn_1()
                    except Exception as e:
                        print e

                    count += 1
            else:
                print "not needed to turn on, not the right time"

        if count == 0:
            print "turning off"
            # pmcontroller turning off
            try:
                pmcontroller.turnOff_1()
            except Exception as e:
                print e


    def autoRunSchedules(self):
        print "auto run"
        while self.running == True:

            self.getAllSchedules()
            self.setTodaysSchedules()

            self.runThroughTodaysSchedules()

            time.sleep(self.pauseInterval)








