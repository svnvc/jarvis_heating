import speech_recognition as sr
import gtts
import os
import time
from voice import Voice



#initialize voice class for computer speech
voice = Voice()

#initialize Pi Mote controller object
pmcontroller = None

# NEED TO UNCOMMENT TO RUN SOCKETS ON RASPBERRY
"""try:
    from pmcontroller import PMController
    pmcontroller = PMController()
except Exception as e:
    print "something went wrong when setting up PMController"
    print e
    print "now quitting program, as module is needed."
    quit()"""

# for turning on port 1: use pmcontroller.turnOn_1()
# turning off port 1: pmcontroller.turnOff_1()



r = sr.Recognizer()

mic = sr.Microphone()

while True or KeyboardInterrupt:
    with mic as source:
        print "listening..."
        r.adjust_for_ambient_noise(source)
        audio = r.listen(source)

        try:
            command = r.recognize_google(audio)
            command = command.lower()       # set to lowercase
            print command

            #voice.sayCustomMessage(command)

            if "hi jarvis" in command:
                voice.sayHi()

            if "heating" in command:

                if "on" in command:
                    pmcontroller.turnOn_1()
                if "off" in command:
                    pmcontroller.turnOff_1()

                voice.sayUnsatisfied()
                print "heating detected!"
                splitted = command.split(" ")
                for item in splitted:
                    try:
                        int(item)
                        print "setting temp to: %s" %int(item)
                    except Exception as e:
                        pass

                    #print item," ", type(item)

            if "hot" in command:
                voice.sayUserHot()

            if "cold" in command:
                voice.sayUserCold()

            if "repeat after me" in command:

                voice.sayTellMeYourSecret()
                time.sleep(2)
                repeat = r.listen(source)
                voice.recordSecretMessage(r.recognize_google(repeat))
                voice.saySecretSafe()

            if "play secret message" in command:
                voice.playSecretMessage()

            if "go to sleep" in command:
                voice.sayCustomMessage("Goodbye, until next time!")
                quit()

        except Exception as e:
            print e



