import sqlite3


class DatabaseController:

    def __init__(self, filename):
        self.db = filename
        self._create_tables()


    def _create_tables(self):
        commands = []
        commands.append("CREATE TABLE IF NOT EXISTS schedules(id INTEGER PRIMARY KEY, day INTEGER, starttime TIME, endtime TIME, temperature INTEGER, socket INTEGER)")

        for command in commands:
            self.execute(command)

    def addSchedule(self, day, starttime, endtime, temperature, socket):
        command = """
                    INSERT INTO schedules(day, starttime, endtime, temperature, socket)
                    VALUES (?, ?, ?, ?, ?)"""
        return self.execute(command,args=(day, starttime, endtime, temperature, socket,))

    def getAll(self):
        command = """ SELECT * FROM schedules"""
        return self.execute(command)


    def deleteScheduleByID(self,id):
        print "id: %s" %id
        command = """DELETE FROM schedules WHERE id=?"""

        return self.execute(command, (id,))

    def execute(self, command, args=()):

        conn = sqlite3.connect(self.db)
        cursor = conn.cursor()
        cursor.execute(command, args)
        result = cursor.fetchall()
        conn.commit()
        conn.close()

        return result
