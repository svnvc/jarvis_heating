from flask import jsonify

#class to intiate responses


class CallResponse:
    def __init__(self):
        self.successBool = False
        self.message = None
        self.data = None

    def getDict(self, message, data):
        self.message = message
        self.data = data
        dict = {"success": self.successBool,
                "message": self.message,
                "data": self.data}

        return dict

    def success(self, message, data=None):
        self.successBool = True

        return jsonify(self.getDict(message, data))

    def fail(self, message, data=None):
        return jsonify(self.getDict(message, data))