import time
import datetime
from jarvis_heating.webapp.modules.mod_heating.routes import tempsett
from jarvis_heating.webapp.modules.mod_heating.routes import getroomtemp
from threading import Thread
 #will work when running from app


#initialize pmcontroller
pmcontroller = None

try:
    from src.pmcontroller import PMController
    pmcontroller = PMController()
except Exception as e:
    print "couldn't load pmcontroller"
    print e


class Tempsett:

    def __init__(self, db):
        self.startAdvance = 0 #in minutes, start before starttime
        self.endAdvance = 0.25 #in minutes, stop before endtime
        self.db = db
        self.running = True
        self.todaysWeekday = datetime.datetime.today().weekday()
        self.pauseInterval = 1800 #seconds in between calls


    def stop(self):
        self.running = False

    def getAllTempsett(self):
            self.allTempsett = self.db.getAll()


    def runTempsett(self):
            print ("running smart-mode")

            now = datetime.datetime.now().time()

            count = 0  # check how many times the time was checked

                # get parameters
                starttime = now + 0.5[2]
                endtime =
                temp = tempsett.tempset[4]
                socket = schedule[5]

                # calculate when to start, beforehand, to have the right temperature when needed
                adj_starttime = (datetime.datetime.strptime(starttime, "%H:%M") - datetime.timedelta(
                    minutes=self.startAdvance)).time()
                print "adj starttime is %s" % adj_starttime

                adj_endtime = (datetime.datetime.strptime(endtime, "%H:%M") - datetime.timedelta(
                    minutes=self.endAdvance)).time()
                print "adj endtime is %s, %s" % (adj_endtime, type(adj_endtime))

                print "starttime: %s" % starttime
                print "endtime: %s" % endtime

                # run through all schedules, and turn on if needed

                if now > adj_starttime and now < adj_endtime:
                    if  getroomtemp < tempsett.tempset :  # place holder for checking current temp regarding target temp ex. currenttemp < targettemp

                        print "turning on!"
                        # include pmcontroller call for on
                        try:
                            pmcontroller.turnOn_1()
                        except Exception as e:
                            print e

                        count += 1
                else:
                    print "not needed to turn on, not the right time"

            if count == 0:
                print "turning off"
                # pmcontroller turning off
                try:
                    pmcontroller.turnOff_1()
                except Exception as e:
                    print e

        def autoRunTempsett(self):
            print "auto running"
            while self.running == True:
                self.getAllTempsett()

                self.runTempsett()

                time.sleep(self.pauseInterval)

