**Jarvis Heating**

Our Jarvis for Heating package brings the ease and comfort of voice activation home software to you. The Jarvis package comes pre-installed with the ability to moderate temperature and duration, for the heating elements of our Next Generation Ink-Kit.
The Jarvis system takes advantage of the Energenie Pi-mote RF module, and sockets, to control up to 4 wall-plugs wirelessly, without wifi or bluetooth. 

Jarvis comes pre-programmed with a set of voice commands that link to the Energenie Pi-mote and turn the heating on or off according to your instructions. If you're tech savy, have a look at our github for resources on how to set the Jarvis system up on your RaspPi (to change the voice commands or to connect to your local Wifi/bluetooth network) and begin controlling Wifi/bluetooth enabled devices. Such as youtube videos, tv shows, radio, coffee machines, you name it!    


---

## Setup
To install the Jarvis software into your Pi-mote please read below:




For a nice guide on how to install the Jarvis software files and folders - our github repository is here:

For a guide on installing more functionality and building your own Jarvis System, purchase our complete ebook guide to the Jarvis system here: 

## Requirements
Made to run on the raspberry pi.

---
