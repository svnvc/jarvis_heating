from flask import render_template, request, redirect, url_for, jsonify, Blueprint, json
from dbcontroller import DatabaseController
import os
from schedulerunner import ScheduleRunner
from Smartmoderunner import Tempsett
from threading import Thread
import time
import RPi.GPIO as GPIO
from src.responder import CallResponse
import urllib3.contrib.pyopenssl
urllib3.contrib.pyopenssl.inject_into_urllib3()
import certifi
import urllib3
urllib3.disable_warnings()
http = urllib3.PoolManager(
cert_reqs='CERT_NOT_REQUIRED',
ca_certs=certifi.where())


mod = Blueprint("mod_heating", __name__, url_prefix="/home++/heating")

# init database
db = DatabaseController("heating.db")

# init pmcontroller
pmcontroller = None

try:
    from src.pmcontroller import PMController
    pmcontroller = PMController()
except Exception as e:
    print "couldn't load pmcontroller"
    print e

# init responder
responder = CallResponse()

#schedule runner
scheduleRunner = None
backgroundRunners = []

#smartmode runner
Tempsettrunner = None
smartmodeRunners = []


@mod.route("/")
def testing():
    return "testing page </br> %s" %os.getcwd()


@mod.route("/getjson/")
def getjson():
    return responder.success("this is a custom message form flask")


@mod.route("/turnon/")
def turnOnHeating():
    message = "Socket 1 turned on"

    try:
        pmcontroller.turnOn_1()
    except Exception as e:
        print e
        message = "Something went wrong: %s" %e
        return responder.fail(message)

    return responder.success(message)


@mod.route("/turnoff/")
def turnOffHeating():
    message = "Socket 1 turned off"

    try:
        pmcontroller.turnOff_1()
    except Exception as e:
        print e
        message = "Something went wrong: %s" % e
        return responder.fail(message)

    return responder.success(message)


@mod.route("/add/")
def addSchedule():
    db.addSchedule(1, "9:00", "10:00", 20, 1)

    db.addSchedule(1, "12:00", "14:00", 20, 1)
    db.addSchedule(4, "17:45", "20:00", 20, 1)

    return "done"

@mod.route("/startschedulerunner/")
def runSchedule():
    print "schedulerunner running"
    scheduleRunner = ScheduleRunner(db)

    t = Thread(target=scheduleRunner.autoRunSchedules)
    t.start()

    backgroundRunners.append(scheduleRunner)

    return "schedulerunner running"


@mod.route("/stopschedulerunner/")
def stopSchedule():
    toDelete = []
    try:
        for item in backgroundRunners:
            item.stop()
            toDelete.append(item)

        for item in toDelete:
            backgroundRunners.remove(item)
        print "background process cleared: %s" % backgroundRunners
    except Exception as e:
        print e
        return "smth went wrong"

    return "stopped"


@mod.route("/scheduleplanner/")
def schedulePlanner():
    allSchedules = db.getAll()
    return render_template("scheduleplanner.html", data = allSchedules)


@mod.route("/scheduleplanner/", methods=["POST"])
def editSchedule():
    action = request.form['action']

    if action == "delete":
        print "deleted detected"
        idToDelete = request.form['id']
        print "id to delete: %s" %idToDelete

        db.deleteScheduleByID(idToDelete)

    if action == "add":
        weekday = request.form["weekday"]
        starttime = request.form["starttime"]
        endtime = request.form["endtime"]
        temperature = request.form["temperature"]
        socket = request.form["socket"]

        db.addSchedule(weekday,starttime,endtime,temperature,socket)


    return redirect(url_for("mod_heating.schedulePlanner"))


@mod.route("/tempsett/")
def tempsett():

    print "Smart-mode activated"

    smartmoderunner = Tempsett()

    t = Thread(target=smartmoderunner.startSmartMode)
    t.start()

    smartmodeRunners.append(smartmoderunner)
    #return "smartmode running, active runners: %s"%smartmodeRunners

    return responder.success("Smart mode activated")


@mod.route("/stopsmartmode/")
def stopsmartmode():
    print "smartmoderunners: %s" %smartmodeRunners

    toDelete = []

    for s in smartmodeRunners:
        print "s: %s" %s
        s.stop()
        toDelete.append(s)

    for item in toDelete:
        smartmodeRunners.remove(item)
        "%s deleted from smartmoderunnerslist" %item

    pmcontroller.turnOff_1()
    return responder.success("smart mode deactivated.")

@mod.route("/getroomtemperature/")
def getroomtemperature():
    sm = Tempsett()
    temp = sm.read_temp()
    temp = round(temp, 1)

    return responder.success("The room temperature is currently %s degrees celsius" % temp)


@mod.route("/targettempp/<targettemp>")
def target(targettemp):
    print "Smart-mode activated"

    smartmoderunner = Tempsett()
    smartmoderunner.setParams(targettemp) # set target temperature to the one given by user
    print "target temperature for smart mode set to %s" %targettemp
    
    t = Thread(target=smartmoderunner.startSmartMode)
    t.start()

    smartmodeRunners.append(smartmoderunner)
    #return "smartmode running, active runners: %s"%smartmodeRunners

    return responder.success("Smart mode activated for %s degrees target temperature" %targettemp)













