import os
import time
import datetime
import routes
from flask import Blueprint, render_template, request, redirect, url_for
from dbcontroller import DatabaseController
from schedulerunner import ScheduleRunner

pmcontroller = None

try:
    from w1thermsensor import W1ThermSensor


    from threading import Thread
     #will work when running from app

    #initialize pmcontroller
    pmcontroller = None

    from src.pmcontroller import PMController

    pmcontroller = PMController()
    sensor = W1ThermSensor()
    temperature_in_celsius = sensor.get_temperature(W1ThermSensor.DEGREES_C)
    temperature_in_fahrenheit = sensor.get_temperature(W1ThermSensor.DEGREES_F)
    temperature_in_all_units = sensor.get_temperatures([
        W1ThermSensor.DEGREES_C,
        W1ThermSensor.DEGREES_F,
        W1ThermSensor.KELVIN])
except Exception as e:
    print e




os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

sensor = '/sys/bus/w1/devices/28-031197795f59/w1_slave'
sensor = os.path.abspath(sensor)


class Tempsett:


    def __init__(self):

        self.target_temperature = 28.00

        self.running = True

        self.today = datetime.datetime.now().time()
        self.pauseInterval = 8  #seconds in between calls
        
    def setParams(self,targetTemp):
        self.target_temperature = float(targetTemp)

    def stop(self):
        print "reached stop mode in Smartmoderunner"

        self.running = False
        pmcontroller.turnOff_1()
        print "Stopped smart mode runner by setting running to false."

    def startSmartMode(self):
        self.running = True

        while self.running == True:
            if self.running == False:
                self.stop()
                break
            self.runtempset()
            time.sleep(self.pauseInterval)

    def runtempset(self):
        """
        Checks room temperature. If room temperature is under the target temperature, it turns on heating,
        if room temperature over or equals target temperature, it turns off heating
        :return:
        """
        print "running smart-mode"

        #tempset = 25
        #print "you have set the room temperature to", tempset, "degrees"
        room_temperature = self.read_temp()
        #if target temperature is reached, shut off for set pause interval
        if room_temperature >= self.target_temperature:
            print "target temperature achieved, turning off socket"

        # if target temperature is reached, we shut off the heating
            try:
                pmcontroller.turnOff_1()
            except Exception as e:
                print e
            time.sleep(self.pauseInterval)

        else:
            print "turning on!"
            try:
                pmcontroller.turnOn_1()
            except Exception as e:
                print e



    def autoRunTempsett(self):
        print "auto running"
        while self.running == True:
            self._temp_raw()
            self.read_temp()
            self.startSmartMode()
            self.runtempset()
            time.sleep(self.pauseInterval)

    def _temp_raw(self):
        f = open(sensor, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_temp(self):

        temperature_celsius = 0.00

        lines = self._temp_raw()

        while lines[0].strip()[-3:] != 'YES':
            time.sleep(10)
            lines = self._temp_raw()

        temp_output = lines[1].find('t=')

        if temp_output != -1:
            temp_string = lines[1].strip()[temp_output + 2:]
            temperature_celsius = float(temp_string) / 1000.0

        print "room temperature measured is %s degrees Celsius." % temperature_celsius

        return temperature_celsius

        #return "%s" % self.read_temp()



