from flask import Flask, jsonify, request, json, render_template
import time
import threading
from src.responder import CallResponse
from flask_cors import CORS
import urllib3.contrib.pyopenssl
urllib3.contrib.pyopenssl.inject_into_urllib3()
import certifi
import urllib3
urllib3.disable_warnings()
http = urllib3.PoolManager(
cert_reqs='CERT_NOT_REQUIRED',
ca_certs=certifi.where())
# initialize flask web app
app = Flask(__name__)
CORS(app)
# register modules
import modules.mod_heating.routes as mod_heating

app.register_blueprint(mod_heating.mod)

# initialize pmcontroller (pi mote controller)
pmcontroller = None
# initialize response class
responder = CallResponse()

try:
    from src.pmcontroller import PMController

    pmcontroller = PMController()
except Exception as e:
    print "couldn't load pmcontroller"
    print e


@app.route("/home")
def home():
    print "home is activated because of visit!"
    message = "Home successfully reached"
    return responder.success(message)


@app.route("/jarvis/heating/on")
def turnOnHeating():
    message = "Socket 1 turned on"
    try:
        pmcontroller.turnOn_1()
    except Exception as e:
        print e
        message = "Something went wrong: %s" % e
        return responder.fail(message)

    return responder.success(message)


@app.route("/jarvis/heating/off")
def turnOffHeating():
    message = "Socket 1 turned off"
    try:
        pmcontroller.turnOff_1()
    except Exception as e:
        print e
        message = "Something went wrong: %s" % e
        return responder.fail(message)

    return responder.success(message)


@app.route("/jarvis/heating/onauto")
def turnOnAuto():
    interval = 20
    print "turn on auto; turn off in %s s" % interval
    message = "turn on auto; turn off in %s s" % interval

    try:
        pmcontroller.turnOn_1()
        print "turned on 1"
    except Exception as e:
        print e

        message = "something went wrong %s" % e
        return responder.fail(message)

    def turnOffAfterTime():
        print "countdown from 20"
        for i in range(0, interval):
            print "i"
            time.sleep(1)

        pmcontroller.turnOff_1()
        print "turned off"

    t = threading.Thread(target=turnOffAfterTime)
    t.start()

    return responder.success(message)


@app.route("/jarvis/heating/onauto/<intervalsecs>")
def turnOnAutoCustom(intervalsecs):
    interval = int(intervalsecs)
    print "interval received: %s" % interval
    print "type of var: %s" % type(interval)

    print "turn on auto; turn off in %s s" % interval
    message = "turn on auto; turn off in %s s" % interval

    try:
        pmcontroller.turnOn_1()
        print "turned on 1"
    except Exception as e:
        print e
        message = "something went wrong %s" % e
        return responder.fail(message)

    def turnOffAfterTime():
        print "countdown from 20"
        for i in range(0, interval):
            print "i"
            time.sleep(1)

        pmcontroller.turnOff_1()
        print "turned off"

    t = threading.Thread(target=turnOffAfterTime)
    t.start()

    return responder.success(message)


@app.route("/postexercise", methods=["POST", "GET"])
def postex():
    res = ""
    for i in request.args:
        res += "%s : %s </br>" % (i, request.args[i])

    return "results:</br> %s" % res


# Zac's function
@app.route('/getroomTemp/')
def getroomTemp():

    import os
    import time

    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')

    temp_sensor = '/sys/bus/w1/devices/28-031197795f59/w1_slave'
    temp_sensor = os.path.abspath(temp_sensor)

    def temp_raw():
        f = open(temp_sensor, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_temp():
        lines = temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(10)
            lines = temp_raw()
        temp_output = lines[1].find('t=')
        if temp_output != -1:
            temp_string = lines[1].strip()[temp_output + 2:]
            temp_c = float(temp_string) / 1000.0
            return temp_c

    return "%s" % read_temp()


if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0")
