#import the required modules
import RPi.GPIO as GPIO
import time

#format from the original ener002 py file, now put into class object.


"""# The On/Off code pairs correspond to the hand controller codes.
# True = '1', False ='0'

print "OUT OF THE BOX: Plug the Pi Transmitter board into the Raspberry Pi"
print "GPIO pin-header ensuring correct polarity and pin alignment."
print ""
print "The sockets will need to be inserted into separate mains wall sockets."
print "with a physical separation of at least 2 metres to ensure they don't"
print "interfere with each other. Do not put into a single extension lead."
print ""
print "For proper set up the sockets should be in their factory state with"
print "the red led flashing at 1 second intervals. If this is not the case for"
print "either socket, press and hold the green button on the front of the unit"
print "for 5 seconds or more until the red light flashes slowly."
print ""
print "A socket in learning mode will be listening for a control code to be"
print "sent from a transmitter. A socket can pair with up to 2 transmitters"
print "and will accept the following code pairs"
print ""
print "0011 and 1011 all sockets ON and OFF"
print "1111 and 0111 socket 1 ON and OFF"
print "1110 and 0110 socket 2 ON and OFF"
print "1101 and 0101 socket 3 ON and OFF"
print "1100 and 0100 socket 4 ON and OFF"
print ""
print "A socket in learning mode should accept the first code it receives"
print "If you wish the sockets to react to different codes, plug in and"
print "program first one socket then the other using this program."
print ""
print "When the code is accepted you will see the red lamp on the socket"
print "flash quickly then extinguish"
print ""
print "The program will now loop around sending codes as follows when you"
print "hit a key:"
print "socket 1 on"
print "socket 1 off"
print "socket 2 on"
print "socket 2 off"
print "all sockets on"
print "all sockets off"
print "Hit CTL C for a clean exit"""

class PMController:

    def __init__(self):

        # set the pins numbering mode
        GPIO.setmode(GPIO.BOARD)

        # Select the GPIO pins used for the encoder K0-K3 data inputs
        GPIO.setup(11, GPIO.OUT)
        GPIO.setup(15, GPIO.OUT)
        GPIO.setup(16, GPIO.OUT)
        GPIO.setup(13, GPIO.OUT)

        # Select the signal to select ASK/FSK
        GPIO.setup(18, GPIO.OUT)

        # Select the signal used to enable/disable the modulator
        GPIO.setup(22, GPIO.OUT)

        # Disable the modulator by setting CE pin lo
        GPIO.output(22, False)

        # Set the modulator to ASK for On Off Keying
        # by setting MODSEL pin lo
        GPIO.output(18, False)

        # Initialise K0-K3 inputs of the encoder to 0000
        GPIO.output(11, False)
        GPIO.output(15, False)
        GPIO.output(16, False)
        GPIO.output(13, False)


    def turnOn_1(self):
        print "sending code 1111 socket 1 on"

        #setpins method returns boolean of success.
        success = self.setPins(pin11=True, pin15=True, pin16=True, pin13=True )

        return success

    def turnOff_1(self):
        """
        turning off socket 1, binary code 0111

        pin 11 = True,
        pin 15 = True,
        pin 16 = True,
        pin 13 = False

        :return:
        """
        print "sending code 0111 Socket 1 off"

        # setpins method returns boolean of success.
        success = self.setPins(pin11=True, pin15=True, pin16=True, pin13=False)

        return success



    def turnOn_2(self):
        print "sending code 1110 socket 2 on"

        success = self.setPins(pin11=False, pin15=True, pin16=True, pin13=True)
        return success

    def turnOff_2(self):
        print "sending code 0110 socket 2 off"

        success = self.setPins(pin11=False, pin15=True, pin16= True, pin13=False)
        return success


    def turnOn_ALL(self):
        print "sending code 1011 ALL on"

        success = self.setPins(pin11=True, pin15=True, pin16=False, pin13=True)
        return success

    def turnOff_ALL(self):
        print "sending code 0011 All off"

        success = self.setPins(pin11=True, pin15=True, pin16=False, pin13=False)
        return success


    def setPins(self, **kwargs):
        """
        Function to set a variable number of pins to the desired value of the rpi. NEEDED INPUT FORMAT:
        example:
        setPins(pin11 = False, pin3= True, pin 22 = True)
        :param kwargs: needs pin number and value, for example pin11 = True, pin3 = False.
        :return:
        """
        for key in kwargs.keys():

            #kwargs come in dictionary, so get out the pin number and the value of it

            print key, " ", kwargs[key]

            pin = int(key.replace("pin", ""))
            value = kwargs[key]


            try:
                GPIO.output(pin, value)

            except Exception as e:
                print "could not set pin: %s to: %s" %(pin, value)
                print e
                return False

        print "All pins have been set."

        #according to original product code, pins need some time to settle and to run modulator.
        self._settleAndModulate()

        return True


    def _settleAndModulate(self):
        """
        from the original ener002-2PI file, this was included. Apparantly need to set the modulater,
        but at this point I Don't know yet what it is for.
        this comes after setting the pins
        :return:
        """

        # let it settle, encoder requires this
        time.sleep(0.1)
        # Enable the modulator
        GPIO.output(22, True)
        # keep enabled for a period
        time.sleep(0.25)
        # Disable the modulator
        GPIO.output(22, False)


    def resetPins(self):
        GPIO.cleanup()





