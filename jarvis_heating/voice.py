import gtts
import os
import sys

class Voice:


    messages = [{"title": "hello.mp3",
                 "text": "Hello, Jarvis reporting for duty sir"},
                {"title": "toohot.mp3",
                 "text": "I agree, too hot here!"},
                {"title": "toocold.mp3",
                 "text": "Brrr, chilly!"},
                {"title": "custom.mp3",
                 "text": "Surprise!"},
                {"title": "unsatisfied.mp3",
                 "text": "Never happy are you?"},
                {"title": "tellme.mp3",
                 "text": "Tell me your secret..."},
                {"title": "secretsafe.mp3",
                 "text": "Your secret is safe with me."},


                ]


    def __init__(self):


        self.playCommand = None

        # checking operating system to determine which play command to run
        operatingSys = sys.platform.lower()

        detected = ""
        if "win32" in operatingSys:
            detected = "windows"
            self.playCommand = "start"

        elif "darwin" in operatingSys:
            detected = "mac"
            self.playCommand = "afplay"

        elif "linux" in operatingSys:
            detected = "linux"
            self.playCommand = "mpg123"
        else:
            detected = "unknown"

        print "setting up system for %s" %detected

        print "setting up voice environment..."
        self.createVoiceFiles()

        print "voice environment ready."

    def sayHi(self):
        file = "hello.mp3"
        self.playSound(file)

    def sayUserCold(self):
        file = "toocold.mp3"
        self.playSound(file)


    def sayUserHot(self):
        file = "toohot.mp3"
        self.playSound(file)


    def sayUnsatisfied(self):
        file = "unsatisfied.mp3"
        self.playSound(file)


    def sayCustomMessage(self, message):
        custom = gtts.gTTS(message)
        file = "custom.mp3"
        custom.save(file)

        self.playSound(file)

    def recordSecretMessage(self, secretMessage):
        secret = gtts.gTTS(secretMessage)
        secret.save("secret.mp3")
        print "secret recorded."

    def playSecretMessage(self):
        self.playSound("secret.mp3")

    def sayTellMeYourSecret(self):
        self.playSound("tellme.mp3")

    def saySecretSafe(self):
        self.playSound("secretsafe.mp3")

    def playSound(self, file):
        os.system("%s %s" %(self.playCommand, file))

    def createVoiceFiles(self):


        for message in self.messages:
            obj = gtts.gTTS(message["text"])
            obj.save(message["title"])

